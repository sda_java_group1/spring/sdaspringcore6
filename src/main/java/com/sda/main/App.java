package com.sda.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sda.model.Speaker;

@ComponentScan(basePackages = {"com.sda"})
public class App {

	public static void main(String[] args) {
		// ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean-configuration.xml");
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(App.class);
		
		// Speaker englishSpeaker = context.getBean(EnglishSpeaker.class);
		Speaker englishSpeaker = context.getBean("englishSpeaker", Speaker.class);
		englishSpeaker.sayHello();

		Speaker frenchSpeaker = context.getBean("frenchSpeaker", Speaker.class);
		frenchSpeaker.sayHello();
		
		context.close();
	}

}
