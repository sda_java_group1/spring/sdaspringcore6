package com.sda.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FrenchSpeaker implements Speaker {

	@Autowired
	@Qualifier("fileWriter")
	private Writer writer;

	public void sayHello() {
		writer.writeText("Bonjour");
	}
	
	@PostConstruct
	public void initialized() {
		System.out.println("FrenchSpeaker initialized");
	}

	@PreDestroy
	public void destroyed() {
		System.out.println("FrenchSpeaker destroyed");
	}
}
