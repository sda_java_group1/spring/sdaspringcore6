package com.sda.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class EnglishSpeaker implements Speaker, InitializingBean, DisposableBean {
	
	@Autowired
	@Qualifier("consoleWriter")
	private Writer writer;
	
//	@Autowired
//	private ConsoleWriter writer;
	
//	@Autowired
//	private Writer consoleWriter;

	public void sayHello() {
		// writer.writeText("Hello");
		writer.writeText("Hello");
	}

	public void destroy() throws Exception {
		System.out.println("EnglishSpeaker destroyed");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("EnglishSpeaker initialized");
	}

}
