package com.sda.model;

import org.springframework.stereotype.Component;

// @Component("cWriter")
@Component
public class ConsoleWriter implements Writer {

	public void writeText(String text) {
		System.out.println(text);
	}

}
